# BAO Linux

## Description
BAO Linux est un outil web qui permet de remplacer certaines fonctionnalités de la Boîte à Outils ATOS des réseaux des lycées de la région Auvergne Rhône Alpes afin de pouvoir utiliser cette boîte à outils sur des clients linux

Actuellement, elle permet uniquement la gestion des mots de passe par les enseignants et les référents numériques

![Gestion des mots de passe](img/screenshot.png)

## Installation
Cette application est destinée à être installé sur un serveur avec PHP et le gestionnaire de template Twig.

Une fois installé, il faut remplir le fichier de configuration `config.template.php` et le renommer `config.php`

## Support
Ce projet est fourni "tel quel", sans aucune garantie. En cas de problème n'hésitez pas à contacter l'auteur ou à ouvrir un ticket dans ce dépot git.

## Roadmap
Ajouter la bibliothèque JSEncrypt pour sécuriser la transmission des mots de passe par HTTP ([voir ce document](https://medium.com/@viniciusamparo/a-simple-guide-to-client-side-encryption-and-decryption-using-javascript-jsencrypt-and-php-20c2f179b6e5))

## Contributing
Si vous avez des proposition d'amélioration, d'extenstion, n'hésitez pas à ouvrir un ticket voir à fournir des merge request !

## License
L'ensemble du projet est sous license GPL3

