<?php

require_once 'config.php';
require_once 'gui.php';

function ADUnicodePwdValue($plain_txt_value)
{
    // This requires recode to be installed on your webserver
    // If this isn't possible, look up alternate ways of formatting unicodePwd in PHP
    return str_replace("\n", "", shell_exec("echo -n '\"" . $plain_txt_value . "\"' | recode latin1..utf-16le/base64"));
}

function change_user_pwd($user, $password)
{
    global $ldap_server, $domain, $users_dn, $admin_user, $admin_pwd, $vars;

    if (strlen($password) >= 8) {
        $ad = ldap_connect($ldap_server);
        ldap_set_option($ad, LDAP_OPT_PROTOCOL_VERSION, 3);
        ldap_set_option($ad, LDAP_OPT_REFERRALS, 0);
        @$bd = ldap_bind($ad, "$admin_user@$domain", "$admin_pwd");

        $user_dn = "CN=$user,$users_dn";
        $entry["unicodePwd"] = iconv("UTF-8", "UTF-16LE", '"' . $password . '"');
        if (ldap_mod_replace($ad, $user_dn, $entry)) {
            succes("Modification du mot de passe pour l'utilisateur $user faîte avec succès");
        } else {
            erreur("Échec de la modification du mot de passe pour l'utilisateur $user : " . ldap_error($ad));
        }
        ldap_close($ad);
    } else {
        erreur("Échec de la modification du mot de passe pour l'utilisateur $user. <br> Le mot de passe doit faire au moins 8 caractères.");
    }
}

function entry2array($entry)
{
    $detail = [];
    $detail["dn"] = $entry["dn"];
    $detail["cn"] = $entry["cn"][0];
    $detail["nom"] = $entry["sn"][0];
    $detail["prenom"] = $entry["givenname"][0];
    $detail["groups"] = [];
    for ($j = 0; $j < $entry["memberof"]["count"]; $j++) {
        $group = explode(',', $entry["memberof"][$j])[0];
        $group = explode("=", $group)[1];
        $detail["groups"][$j] = strtolower($group);
    }

    return $detail;
}

function user_details($identifiant)
{
    global $ldap_server, $domain, $dc, $admin_user, $admin_pwd;
    $ad = ldap_connect($ldap_server);
    // Définissons quelques variables...
    ldap_set_option($ad, LDAP_OPT_PROTOCOL_VERSION, 3);
    ldap_set_option($ad, LDAP_OPT_REFERRALS, 0);
    @$bd = ldap_bind($ad, "$admin_user@$domain", "$admin_pwd");
    $sr = ldap_search($ad, "DC=" . $dc . ",DC=etab,DC=local", "(&(objectclass=person)(cn=$identifiant))");
    $info = ldap_get_entries($ad, $sr);
    $detail = entry2array($info[0]);
    return $detail;
}

function is_member_of($identifiant, $group)
{
    global $ldap_server, $domain, $dc, $admin_user, $admin_pwd, $groups_dn;
    $ad = ldap_connect($ldap_server);
    // Définissons quelques variables...
    ldap_set_option($ad, LDAP_OPT_PROTOCOL_VERSION, 3);
    ldap_set_option($ad, LDAP_OPT_REFERRALS, 0);
    @$bd = ldap_bind($ad, "$admin_user@$domain", "$admin_pwd");
    $sr = ldap_search($ad, "DC=" . $dc . "DC=etab,DC=local", "(&(objectclass=person)(cn=$identifiant))");
    $info = ldap_get_entries($ad, $sr);
    $detail = entry2array($info[0]);
    return (in_array(strtolower($group), $detail['groups'], true));
}

function search_pupils($name)
{
    global $ldap_server, $domain, $dc, $admin_user, $admin_pwd;
    $ad = ldap_connect($ldap_server);
    // Définissons quelques variables...
    ldap_set_option($ad, LDAP_OPT_PROTOCOL_VERSION, 3);
    ldap_set_option($ad, LDAP_OPT_REFERRALS, 0);
    @$bd = ldap_bind($ad, "$admin_user@$domain", "$admin_pwd");
    $sr = ldap_search($ad, "DC=" . $dc .",DC=etab,DC=local", "(|(&(objectclass=person)(cn=*" . $name . "*))(&(objectclass=person)(sn=*" . $name . "*))(&(objectclass=person)(givenname=*" . $name . "*)))");
    if (ldap_errno($ad) != 0) {
        $fail = "Échec de la recherche d'élève" . htmlentities($name) . ": " . ldap_error($ad);
        erreur($fail);
        error_log($fail);
    }
    $info = ldap_get_entries($ad, $sr);
    $pupils = [];
    for ($i = 0; $i < $info["count"]; $i++) {
        $detail = entry2array($info[$i]);
        if (in_array("eleves", $detail['groups'], true) || $_SESSION['isReferent']) {
            $pupils[$detail["cn"]] = $detail;
        }
    }
    return $pupils;
}

function group_detail($name)
{
    global $ldap_server, $domain, $dc, $admin_user, $admin_pwd, $vars;
    $ad = ldap_connect($ldap_server);
    // Définissons quelques variables...
    ldap_set_option($ad, LDAP_OPT_PROTOCOL_VERSION, 3);
    ldap_set_option($ad, LDAP_OPT_REFERRALS, 0);
    @$bd = ldap_bind($ad, "$admin_user@$domain", "$admin_pwd");
    $sr = ldap_search($ad, "DC=" . $dc . ",DC=etab,DC=local", "(&(objectclass=group)(cn=" . $name . "))");
    if (ldap_errno($ad) != 0) {
        $fail = "Échec de la recherche du groupe " . htmlentities($name) . ": " . ldap_error($ad);
        erreur($fail);
        error_log($fail);
    }
    $info = ldap_get_entries($ad, $sr);
    if (ldap_errno($ad) != 0) {
        erreur("Échec lors du parcours du groupe $name : " . ldap_error($ad));
    } else {
        succes("Le groupe $name contient  " . $info[0]["member"]["count"] . " membres ");
        error_log("Le groupe $name contient  " . $info[0]["member"]["count"] . " élèves ");
    }
    $pupils = [];
    for ($i = 0; $i < $info[0]["member"]["count"]; $i++) {
        $cn = explode(',', $info[0]["member"][$i])[0];
        $cn = explode('=', $cn)[1];
        error_log($cn);
        $detail = user_details($cn);
        if (in_array("eleves", $detail['groups'], true) || $_SESSION['isReferent']) {
            $pupils[$cn] = $detail;
            error_log("ajout de $cn");
        }
    }
    return $pupils;
}

function search_groups($name)
{
    global $ldap_server, $domain, $dc, $admin_user, $admin_pwd;
    $ad = ldap_connect($ldap_server);
    // Définissons quelques variables...
    ldap_set_option($ad, LDAP_OPT_PROTOCOL_VERSION, 3);
    ldap_set_option($ad, LDAP_OPT_REFERRALS, 0);
    @$bd = ldap_bind($ad, "$admin_user@$domain", "$admin_pwd");
    $sr = ldap_search($ad, "DC=" . $dc . ",DC=etab,DC=local", "(&(objectclass=group)(cn=*" . $name . "*))");
    if (ldap_errno($ad) != 0) {
        $fail = "Échec de la recherche du groupe " . htmlentities($name) . " : " . ldap_error($ad);
        erreur($fail);
        error_log($fail);
    }
    $info = ldap_get_entries($ad, $sr);
    $groups = [];
    for ($i = 0; $i < $info["count"]; $i++) {
        $detail = [];
        $detail["dn"] = $info[$i]["dn"];
        $detail["cn"] = strtolower($info[$i]["cn"][0]);
        $groups[$detail["cn"]] = $detail;
    }
    return $groups;
}
