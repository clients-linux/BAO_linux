<?php
require_once 'config.php';
require_once 'vendor/autoload.php';
require_once 'ldaputils.php';
session_start();

function identification()
{
    global $ldap_server, $domain, $vars;
    if (!isset($_SESSION['cn']) && isset($_POST['identifiant']) && isset($_POST['password'])) {
        $identifiant = $_POST['identifiant'];
        $password = $_POST['password'];
        // identifiant non vide
        if (strlen(trim($identifiant)) == 0) {
            $vars['erreurs'][] = "l'identifiant ne peut pas être vide";
            return ('identification.twig');
        }
        // Mot de passe non vide
        if (strlen(trim($password)) == 0) {
            $vars['erreurs'][] = "Le mot de passe ne peut pas être vide";
            return ('identification.twig');
        }
        // --- Connexion au serveur AD
        $ad = ldap_connect($ldap_server);
        ldap_set_option($ad, LDAP_OPT_PROTOCOL_VERSION, 3);
        ldap_set_option($ad, LDAP_OPT_REFERRALS, 0);
        // On tente l'authentification 

        @$bd = ldap_bind($ad, "$identifiant@$domain", "$password");
        if ($bd) { // Si authentification ok, redirection vers le menu principal de l'appli
            $_SESSION["cn"] = $identifiant;
            $detail = user_details($identifiant);
            $_SESSION["dn"] = $detail["dn"];
            $_SESSION["nom"] = $detail["nom"];
            $_SESSION["prenom"] = $detail["prenom"];
            $_SESSION["groups"] = $detail["groups"];
            $_SESSION["isReferent"] = in_array("referents", $detail["groups"], true);
            $_SESSION["isProfesseur"] = in_array("professeurs", $detail["groups"], true);
            $_SESSION["cours"] = array();
            if ($_SESSION["isProfesseur"]) {
                foreach ($_SESSION["groups"] as $group) {
                    if (preg_match('/_profs$/i', $group) == 1) {
                        $_SESSION['cours'][] = substr($group, 0, -6) . "_eleves";
                    }
                }
            }
            ldap_close($ad);
            return ('welcome.twig');
        } else {
            $fail = "Echec de l'identification sur le domaine du lycée : " . ldap_error($ad);
            $vars['erreurs'][] = $fail;
            error_log($fail);
            ldap_close($ad);
            return ('identification.twig');
        }
    } else {
        return ('identification.twig');
    }
}

function logout()
{
    global $vars;
    // Détruit toutes les variables de session
    $_SESSION = array();

    // Si vous voulez détruire complètement la session, effacez également
    // le cookie de session.
    // Note : cela détruira la session et pas seulement les données de session !
    if (ini_get("session.use_cookies")) {
        $params = session_get_cookie_params();
        setcookie(
            session_name(),
            '',
            time() - 42000,
            $params["path"],
            $params["domain"],
            $params["secure"],
            $params["httponly"]
        );
    }
    // Finalement, on détruit la session.
    session_destroy();
    $vars['success'][] = "Vous avez été déconnecté avec succès.";
    return ('identification.twig');
}
