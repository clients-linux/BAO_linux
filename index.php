<?php
/*
Pour faire fonctionner
modifier /etc/ldap/ldap.conf
Ajouter la ligne 
TLS_REQCERT never
pour ne pas vérfier le certificat de l'AD mais pouvoir travailler en SSL
*/
require_once('include/auth.php');



$loader = new \Twig\Loader\FilesystemLoader('templates');
$twig = new \Twig\Environment($loader, ['debug' => true, 'cache' => false]);

$action = isset($_POST['action']) ? $_POST['action'] :  'none';

$vars = [];
$vars['erreurs'] = array();
$vars['success'] = array();

$template_file = 'welcome.twig';

if (!isset($_SESSION['cn'])) {
    $template_file = identification();
} else {
    switch ($action) {
        case 'logout':
            $template_file = logout();
            break;
        case 'pupils_search':
            if ($_SESSION['isReferent'] || $_SESSION['isProfesseur']) {
                $vars['pupils'] = search_pupils($_POST['pupil_name']);
                $template_file = 'pupils_search.twig';
            } else {
                $vars['erreurs'][] = " Vous n'êtes pas autorisé à accéder à cet espace. Cet incident a été signalé";
                $template_file = 'welcome.twig';
            }
            break;
        case 'groups_search':
            if ($_SESSION['isReferent'] || $_SESSION['isProfesseur']) {
                $vars['groups'] = search_groups($_POST['group_name']);
                $template_file = 'groups_search.twig';
            } else {
                $vars['erreurs'][] = " Vous n'êtes pas autorisé à accéder à cet espace. Cet incident a été signalé";
                $template_file = 'welcome.twig';
            }
            break;
        case 'group_detail':
            if ($_SESSION['isReferent'] || $_SESSION['isProfesseur']) {
                $vars['pupils'] = group_detail($_POST['group']);
                $template_file = 'group_detail.twig';
            } else {
                $vars['erreurs'][] = " Vous n'êtes pas autorisé à accéder à cet espace. Cet incident a été signalé";
                $template_file = 'welcome.twig';
            }
            break;
        case 'people_pwd':
            if ($_SESSION['isReferent'] || $_SESSION['isProfesseur']) {
                $template_file = 'change_password.twig';
            } else {
                $vars['erreurs'][] = " Vous n'êtes pas autorisé à accéder à cet espace. Cet incident a été signalé";
                $template_file = 'welcome.twig';
            }
            break;
        case 'modify_pwd':
            if ($_SESSION['isReferent'] || $_SESSION['isProfesseur']) {
                change_user_pwd($_POST['user'], $_POST['password']);
                $template_file = 'welcome.twig';
            } else {
                $vars['erreurs'][] = " Vous n'êtes pas autorisé à accéder à cet espace. Cet incident a été signalé";
                $template_file = 'welcome.twig';
            }
            break;
        case 'change_own_pwd':
            change_user_pwd($_SESSION['cn'], $_POST['password']);
            $template_file = 'welcome.twig';
            break;
    };
}

$vars['session'] = $_SESSION;
$vars['post'] = $_POST;

echo $twig->render($template_file, $vars);
